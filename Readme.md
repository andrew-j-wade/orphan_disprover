# orphan_disprover: A program for disproving 4-row orphans in Conway's Game of Life

## Usage

1. Install <https://www.rust-lang.org>

2. Clone the repository.

3. Run "RUSTFLAGS='-C target-cpu=native' cargo build --release". The RUSTFLAGS variable is optional.

4. Run target/release/orphan_disprover data.dat

With default options data.dat will be 8GB. Interrupt with ^C, resume by running the same command line.
Runtime on an eight core machine in 2023 is about 2 months.

For help on arguments run target/release/orphan_disprover --help. If increasing the rows,
reduce the number of context columns to keep the size of the data file reasonable.

The data file will be 2 ** (--rows * --context-columns + 2 * --rows + 4) bits in size.


## The Algorithm

A partial solution to a target pattern is defined as pattern in a rectangular region two rows
higher than the target in the parent generation whose interior cells all match the target pattern
in the target generation for the rows/columns where the target is defined.

Partial solutions thus defined are not necessarily extendible to full solutions.
They also don't necessarily result in empty cells outside the rows of the target
since we are disproving orphans where cells outside the 4 rows are unspecified.

To prove the non-existance of orphans, we find a non-empty proof set, S with the following properties:

IF:

* The last two columns of a partial solution,
along with six columns of corresponding context from a target are in S,

THEN:

* There exists a longer partial solution, whose last two columns,
along with six columns of corresponding context from the target
(possibly padded with empty cells to the right), are also in S.

By induction on the length of the partial solutions you can then show that there must be a
solution long enough to produce entire target for any finite target.

The base case for the induction can be taken care of by noting that any finite target can be
prepended by a context that has a two-column partial solution in S.

An example of going from a partial solution to a longer partial solution:
```
enter target:
 *******************************
 ** * *** * *** * *** * *** * **
 * *** * *** * *** * *** * *** *
 *******************************
enter partial solution:
..**
.*..
*...
.*.*
*.**
....

Precondition:
 **
(.., ******) is a member of the proof set: VERIFIED.
 ..  .*.***
 .*  ***.*.
 **  ******
 ..

Longer partial solution found:
..***.*
.*.....
*....*.
.*.**..
*.**..*
......*

Postcondition:
 .*
(.., ******) is a member of the proof set: VERIFIED.
 *.  ***.*.
 ..  .*.***
 .*  ******
 .*

```

To find the proof set the program starts with a candidate set where all combinations of the last
two columns and contexts are included, and removes members where a further member of the set
cannot be found within a certain number of columns (--search-columns) for some target.
Since the criteria for inclusion into the set does depend on what other elements are in the set,
the program does repeated passes over the candidate set until a fixed point is reached.
If the set is non-empty at that point then orphans have been disproved.
If the set is empty no conclusion can be drawn about the existance of orphans since there is
no base case for induction.

## Gardens of Eden

With the `--garden-of-eden` option the partial solutions are further restricted to not produce any stray cells in two rows above and two rows below the target when the partial solution is run between two cell high "rails". The use of rails was chosen because they are fairy effective at suppressing stray cells in the target generation for B3/S23. Complications terminating the rails can be avoided by extending them to infinity:
```
                          .
                          .
                          .

    **********************************************
    **********************************************


        (solution producing empty target here)



    **********************************************
    **********************************************


...     (solution producing target here)          ...



    **********************************************
    **********************************************


        (solution producing empty target here)



    **********************************************
    **********************************************

                          .
                          .
                          .
```
