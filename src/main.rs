use memmap::MmapMut;
use rayon::prelude::*;

use std::cmp::{max, min};
use std::fs::OpenOptions;
use std::io;
use std::io::BufRead;
use std::mem;
use std::path::PathBuf;
use structopt::StructOpt;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use bitintr::Pdep;

// The SubRule contains the character for the subrule and a list of the coordinates of the outer
// cells (in an arbitrary orientation) for that subrule.

type SubRule = (char, &'static [(i32, i32)]);

const HENSEL_LOOKUP: &[(char, &[SubRule])] = &[
    ('0', &[('x', &[])]),
    ('1', &[('c', &[(-1, -1)]), ('e', &[(0, -1)])]),
    (
        '2',
        &[
            ('c', &[(-1, -1), (1, -1)]),
            ('e', &[(-1, 0), (0, -1)]),
            ('k', &[(0, -1), (1, 1)]),
            ('a', &[(-1, -1), (0, -1)]),
            ('i', &[(0, -1), (0, 1)]),
            ('n', &[(-1, -1), (1, 1)]),
        ],
    ),
    (
        '3',
        &[
            ('c', &[(-1, -1), (1, -1), (1, 1)]),
            ('e', &[(-1, 0), (0, -1), (1, 0)]),
            ('k', &[(-1, 0), (0, -1), (1, 1)]),
            ('a', &[(-1, 0), (-1, -1), (0, -1)]),
            ('i', &[(-1, -1), (-1, 0), (-1, 1)]),
            ('n', &[(-1, -1), (-1, 0), (1, -1)]),
            ('y', &[(-1, -1), (1, -1), (0, 1)]),
            ('q', &[(-1, -1), (-1, 0), (1, 1)]),
            ('j', &[(1, -1), (1, 0), (0, 1)]),
            ('r', &[(0, -1), (1, -1), (0, 1)]),
        ],
    ),
    (
        '4',
        &[
            ('c', &[(-1, -1), (1, -1), (-1, 1), (1, 1)]),
            ('e', &[(-1, 0), (0, -1), (1, 0), (0, 1)]),
            ('k', &[(0, -1), (1, -1), (-1, 0), (1, 1)]),
            ('a', &[(-1, -1), (-1, 0), (-1, 1), (0, 1)]),
            ('i', &[(-1, -1), (1, -1), (-1, 0), (1, 0)]),
            ('n', &[(-1, -1), (-1, 0), (-1, 1), (1, 1)]),
            ('y', &[(-1, -1), (1, -1), (-1, 1), (0, 1)]),
            ('q', &[(-1, -1), (0, -1), (-1, 0), (1, 1)]),
            ('j', &[(1, -1), (-1, 0), (1, 0), (0, 1)]),
            ('r', &[(0, -1), (1, -1), (1, 0), (0, 1)]),
            ('t', &[(-1, -1), (0, -1), (1, -1), (0, 1)]),
            ('w', &[(-1, -1), (-1, 0), (0, 1), (1, 1)]),
            ('z', &[(-1, -1), (0, -1), (0, 1), (1, 1)]),
        ],
    ),
    (
        '5',
        &[
            ('c', &[(0, -1), (-1, 0), (1, 0), (-1, 1), (0, 1)]),
            ('e', &[(-1, -1), (1, -1), (-1, 1), (0, 1), (1, 1)]),
            ('k', &[(-1, -1), (1, -1), (1, 0), (-1, 1), (0, 1)]),
            ('a', &[(1, -1), (1, 0), (-1, 1), (0, 1), (1, 1)]),
            ('i', &[(0, -1), (1, -1), (1, 0), (0, 1), (1, 1)]),
            ('n', &[(0, -1), (1, 0), (-1, 1), (0, 1), (1, 1)]),
            ('y', &[(0, -1), (-1, 0), (1, 0), (-1, 1), (1, 1)]),
            ('q', &[(0, -1), (1, -1), (1, 0), (-1, 1), (0, 1)]),
            ('j', &[(-1, -1), (0, -1), (-1, 0), (-1, 1), (1, 1)]),
            ('r', &[(-1, -1), (-1, 0), (1, 0), (-1, 1), (1, 1)]),
        ],
    ),
    (
        '6',
        &[
            ('c', &[(0, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]),
            ('e', &[(-1, -1), (1, -1), (1, 0), (-1, 1), (0, 1), (1, 1)]),
            ('k', &[(-1, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1)]),
            ('a', &[(1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]),
            ('i', &[(-1, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (1, 1)]),
            ('n', &[(0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1)]),
        ],
    ),
    (
        '7',
        &[
            (
                'c',
                &[(0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)],
            ),
            (
                'e',
                &[(-1, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)],
            ),
        ],
    ),
    (
        '8',
        &[(
            'x',
            &[
                (-1, -1),
                (0, -1),
                (1, -1),
                (-1, 0),
                (1, 0),
                (-1, 1),
                (0, 1),
                (1, 1),
            ],
        )],
    ),
];

/// Takes a Hensel rule string and returns a bit array of all the 3x3 patterns that result in
/// an alive cell in the next generation.
///
fn translate_rule(rule: &str) -> [u8; 64] {
    let mut rv = [0; 64];

    //outer neighborboods for birth and survival rules:
    let mut born_rules = Vec::<&'static [(i32, i32)]>::new();
    let mut survival_rules = Vec::<&'static [(i32, i32)]>::new();

    //If we don't see a 'B' we assume the survial counts come first.
    let mut born_rule = false;

    //Subrule lookup for the current count:
    let mut count_lookup: Option<&[SubRule]> = None;
    let mut subrules: Option<Vec<&SubRule>> = None; // None = no subrules for the count.
    let mut negative_subrules = false; // true once '-' has been encountered.

    // We add an extra '/' to the end of the rule string because the '/' will trigger the
    // saving of the subrules for the last digit.
    for c in rule.chars().chain("/".chars()) {
        if ['B', 'S', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8'].contains(&c) {
            // At this point we know there are no more subrules for the previously processed
            // "digit" and we can add the subrules collected so far to the neighborhood vecs.
            let previous_rule = if born_rule {
                &mut born_rules
            } else {
                &mut survival_rules
            };
            if let Some(subrules) = subrules {
                for (_, lookup) in subrules.iter() {
                    previous_rule.push(lookup);
                }
            } else if let Some(count_lookup) = count_lookup {
                for (_, lookup) in count_lookup.iter() {
                    previous_rule.push(lookup);
                }
            };
            count_lookup = None;
            subrules = None;
            negative_subrules = false;

            for (count, lookup) in HENSEL_LOOKUP.iter() {
                if *count == c {
                    count_lookup = Some(*lookup)
                }
            }
            match c {
                'B' => born_rule = true,
                '/' => born_rule = true,
                'S' => born_rule = false,
                _ => {}
            };
        } else if c == '-' {
            assert!(subrules.is_none());
            // Add all the subrules.
            // They will be removed as they are encountered in the rule string.
            subrules = Some(count_lookup.unwrap().iter().collect::<Vec<_>>());
            negative_subrules = true;
        } else {
            //confirm subrule is valid.
            assert!(count_lookup
                .unwrap()
                .iter()
                .any(|(subrule, _)| *subrule == c));

            if negative_subrules {
                subrules
                    .as_mut()
                    .unwrap()
                    .retain(|(subrule, _)| *subrule != c);
            } else {
                subrules.get_or_insert(Vec::new()).extend(
                    count_lookup
                        .unwrap()
                        .iter()
                        .filter(|(subrule, _)| *subrule == c),
                );
            };
        };
    }

    //Neighborhoods from the lookup are in an arbitrary orientation. Iterate over all the
    //orientations.
    for transform in [
        [[1, 0], [0, 1]],
        [[-1, 0], [0, 1]],
        [[1, 0], [0, -1]],
        [[-1, 0], [0, -1]],
        [[0, 1], [1, 0]],
        [[0, -1], [1, 0]],
        [[0, 1], [-1, 0]],
        [[0, -1], [-1, 0]],
    ]
    .iter()
    {
        for subrule in born_rules.iter() {
            let mut idx = 0;
            for (x, y) in subrule.iter() {
                let (x, y) = (
                    transform[0][0] * x + transform[0][1] * y,
                    transform[1][0] * x + transform[1][1] * y,
                );
                idx |= 1 << (((x + 1) * 3) + y + 1);
            }
            rv[idx >> 3] |= 1 << (idx & 7)
        }
        for subrule in survival_rules.iter() {
            let mut idx = 0b10000;
            for (x, y) in subrule.iter() {
                let (x, y) = (
                    transform[0][0] * x + transform[0][1] * y,
                    transform[1][0] * x + transform[1][1] * y,
                );
                idx |= 1 << (((x + 1) * 3) + y + 1);
            }
            rv[idx >> 3] |= 1 << (idx & 7)
        }
    }

    rv
}

struct Search {
    opt: Opt,
    term: Arc<AtomicBool>,
    num_chunks: usize,
    log2_num_chunks: u32,
    parent_shift: u32,
    cells_shift: u32,
    advance_mask: usize,
    chunk_initialization: u64,
    lookup_chunk_count: usize,
    advance_lookup: Box<[u64]>,
    equivalent_chunk: Box<[u32]>,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum SearchResult {
    Interrupted,
    Failed,
    OrphansDisproved,
}

impl Search {
    fn new(opt: Opt) -> Self {
        let rows = opt.rows;
        let parent_shift = max(rows, 4) - 4;
        let log2_num_chunks = rows + 2 + parent_shift;
        let num_chunks = 1 << (rows + 2 + parent_shift);
        let advance_mask = ((1 << (rows + 2)) - 1) << parent_shift;
        let chunk_initialization = if rows < 4 {
            (1 << (1 << (rows + 2))) - 1
        } else {
            u64::MAX
        };
        let cells_shift = (rows + 2) + 6 + 2 * parent_shift;

        let mut rv = Self {
            opt,
            term: Arc::new(AtomicBool::new(false)),
            num_chunks,
            log2_num_chunks,
            parent_shift,
            cells_shift,
            advance_mask,
            chunk_initialization,
            lookup_chunk_count: 1 << parent_shift,
            advance_lookup: (0..(1 << (cells_shift + rows)))
                .map(|_| 0)
                .collect::<Vec<_>>()
                .into_boxed_slice(),
            equivalent_chunk: (0..num_chunks << rows)
                .map(|chunki| (chunki % num_chunks) as u32)
                .collect::<Vec<_>>()
                .into_boxed_slice(),
        };

        rv.init_lookup();
        rv.init_equivalent_chunk();
        rv
    }

    fn init_lookup(&mut self) {
        let rule_lookup = translate_rule(&self.opt.rule);

        for outside_parent in 0..(1 << (self.opt.rows + 2)) {
            for middle_parent in 0..(1 << (self.opt.rows + 2)) {
                'lookup_parent: for lookup_parent in 0..(1 << (self.opt.rows + 2)) {
                    if self.opt.garden_of_eden {
                        for border in [0, 1, self.opt.rows + 2, self.opt.rows + 3] {
                            if rule_lookup[(((((outside_parent << 2)
                                + 3
                                + (3 << (self.opt.rows + 4)))
                                >> border)
                                & 7)
                                << 3)
                                + ((((middle_parent << 2) + 3 + (3 << (self.opt.rows + 4)))
                                    >> border)
                                    & 7)]
                                & (1 << ((((lookup_parent << 2) + 3 + (3 << (self.opt.rows + 4)))
                                    >> border)
                                    & 7))
                                != 0
                            {
                                // The target generation has alive cells in the border:
                                // Reject this combination.
                                continue 'lookup_parent;
                            }
                        }
                    }
                    let mut idx = 0;
                    for cell in 0..self.opt.rows {
                        if rule_lookup
                            [(((outside_parent >> cell) & 7) << 3) + ((middle_parent >> cell) & 7)]
                            & (1 << ((lookup_parent >> cell) & 7))
                            != 0
                        {
                            idx |= 1 << cell;
                        };
                    }
                    idx <<= self.opt.rows + 2;
                    idx += outside_parent;
                    idx <<= 6 + self.parent_shift;
                    idx += middle_parent;
                    idx <<= 6 + self.parent_shift;
                    idx += lookup_parent;
                    self.advance_lookup[idx >> 6] |= 1 << (idx & 63);
                }
            }
        }
    }

    fn init_equivalent_chunk(&mut self) {
        for cells in 0..(1 << self.opt.rows) {
            'outside_parent: for outside_parent in 0..(1 << (self.opt.rows + 2)) {
                'candidate: for candidate in 0..outside_parent {
                    for middle_parent in 0..(1 << (self.opt.rows + 2)) {
                        let mut addr = cells;
                        addr <<= self.opt.rows + 2;
                        addr += outside_parent;
                        addr <<= 6 + self.parent_shift;
                        addr += middle_parent;
                        addr <<= self.parent_shift;
                        let mut candidate_addr = cells;
                        candidate_addr <<= self.opt.rows + 2;
                        candidate_addr += candidate;
                        candidate_addr <<= 6 + self.parent_shift;
                        candidate_addr += middle_parent;
                        candidate_addr <<= self.parent_shift;
                        if self.advance_lookup[addr..addr + self.lookup_chunk_count]
                            != self.advance_lookup
                                [candidate_addr..candidate_addr + self.lookup_chunk_count]
                        {
                            continue 'candidate;
                        }
                    }
                    // The candidate is equivalent to outside_parent for the purpose of advance()
                    for i in 0..self.lookup_chunk_count {
                        self.equivalent_chunk
                            [cells * self.num_chunks + (outside_parent << self.parent_shift) + i] =
                            ((candidate << self.parent_shift) + i) as u32;
                    }
                    continue 'outside_parent;
                }
            }
        }
    }

    fn advance(&self, cells: u32, src: &[u64], dest: &mut [u64]) {
        for chunk in dest.iter_mut() {
            *chunk = 0;
        }
        let shifted_cells = (cells as usize) << self.cells_shift;

        //rustc 1.54.0 optimizes a loop over lookup_chunk_count poorly
        //so specialize a few cases.
        if self.lookup_chunk_count == 1 {
            let dest = &mut dest[0..=self.advance_mask]; //hoist the bounds check.
            for (chunki, chunk) in src.iter().enumerate() {
                let mut chunk = *chunk;
                while chunk != 0 {
                    let chunk_bit = chunk.trailing_zeros();
                    let addr = shifted_cells + ((chunki << 6) + chunk_bit as usize);
                    let advance_addr = addr & self.advance_mask;

                    dest[advance_addr] |= self.advance_lookup[addr];

                    chunk -= 1 << chunk_bit;
                }
            }
        } else if self.lookup_chunk_count == 2 {
            let dest = &mut dest[0..=self.advance_mask + 1]; //hoist the bounds check.
            for (chunki, chunk) in src.iter().enumerate() {
                let mut chunk = *chunk;
                while chunk != 0 {
                    let chunk_bit = chunk.trailing_zeros();
                    let addr = shifted_cells + (((chunki << 6) + chunk_bit as usize) << 1);
                    let advance_addr = addr & self.advance_mask;

                    dest[advance_addr] |= self.advance_lookup[addr];
                    dest[advance_addr + 1] |= self.advance_lookup[addr + 1];

                    chunk -= 1 << chunk_bit;
                }
            }
        } else if self.lookup_chunk_count == 4 {
            let dest = &mut dest[0..=self.advance_mask + 3]; //hoist the bounds check.
            for (chunki, chunk) in src.iter().enumerate() {
                let mut chunk = *chunk;
                while chunk != 0 {
                    let chunk_bit = chunk.trailing_zeros();
                    let addr = shifted_cells + (((chunki << 6) + chunk_bit as usize) << 2);
                    let advance_addr = addr & self.advance_mask;

                    dest[advance_addr] |= self.advance_lookup[addr];
                    dest[advance_addr + 1] |= self.advance_lookup[addr + 1];
                    dest[advance_addr + 2] |= self.advance_lookup[addr + 2];
                    dest[advance_addr + 3] |= self.advance_lookup[addr + 3];

                    chunk -= 1 << chunk_bit;
                }
            }
        } else {
            let dest = &mut dest[0..=self.advance_mask + self.lookup_chunk_count - 1]; //hoist the bounds check.
            for (chunki, chunk) in src.iter().enumerate() {
                let mut chunk = *chunk;
                while chunk != 0 {
                    let chunk_bit = chunk.trailing_zeros();
                    let addr =
                        shifted_cells + (((chunki << 6) + chunk_bit as usize) << self.parent_shift);
                    let advance_addr = addr & self.advance_mask;
                    for i in 0..self.lookup_chunk_count {
                        dest[advance_addr + i] |= self.advance_lookup[addr + i];
                    }

                    chunk -= 1 << chunk_bit;
                }
            }
        };
    }

    /// For a given context return which context number
    /// it is in the table.
    fn context_index(&self, context: &[u32]) -> usize {
        let mut rv = 0;
        for &cells in context {
            rv <<= self.opt.rows;
            rv += cells as usize;
        }
        rv
    }

    /// For the parents bit array of a context provide a bit array of the parents mirrored.
    fn mirror_parents(&self, parents: &[u64], mirrored_parents: &mut [u64]) {
        if self.opt.rows == 4 {
            //We can special case this with pext pdep
            for chunk_i in 0..64usize {
                let v = parents[chunk_i];
                let v = (v >> 32).pdep(0xAAAA_AAAA_AAAA_AAAA) + v.pdep(0x5555_5555_5555_5555);
                let v = (v >> 32).pdep(0xCCCC_CCCC_CCCC_CCCC) + v.pdep(0x3333_3333_3333_3333);
                let v = (v >> 32).pdep(0xF0F0_F0F0_F0F0_F0F0) + v.pdep(0x0F0F_0F0F_0F0F_0F0F);
                let v = (v >> 32).pdep(0xFF00_FF00_FF00_FF00) + v.pdep(0x00FF_00FF_00FF_00FF);
                let v = (v >> 32).pdep(0xFFFF_0000_FFFF_0000) + v.pdep(0x0000_FFFF_0000_FFFF);
                let dest_i = ((chunk_i as u8).reverse_bits() >> 2) as usize;
                mirrored_parents[dest_i] = v;
            }
            return;
        }
        for v in mirrored_parents.iter_mut() {
            *v = 0;
        }
        for parent in 0..self.num_chunks << 6 {
            if parents[parent >> 6] & (1 << (parent & 63)) != 0 {
                let reversed_parent = parent.reverse_bits();
                let mirrored_parent = (reversed_parent >> (usize::BITS - 2 - self.opt.rows))
                    + ((reversed_parent << (6 + self.parent_shift))
                        >> (usize::BITS - 6 - 2 - self.opt.rows - self.parent_shift));
                mirrored_parents[mirrored_parent >> 6] += 1 << (mirrored_parent & 63);
            };
        }
    }

    /// Returns true if we should remove the parent for this context
    fn keep_parent(&self, target: &[u32], table: &[u64], parent: u32) -> bool {
        let mut target: Box<[u32]> = target.iter().cloned().collect();
        let mut parents = vec![0u64; self.num_chunks * (target.len() + 1)].into_boxed_slice();
        let mut place = self.opt.context_columns;
        let mut parents_place = 1;

        parents[(parent >> 6) as usize] = 1 << (parent & 63);
        target[place] = 0;

        {
            let (root_parents, next_parents) = parents.split_at_mut(self.num_chunks);
            self.advance(
                target[0],
                root_parents,
                &mut next_parents[..self.num_chunks],
            );
        }

        // We will be updating unchecked_leafs as we walk the tree
        // We will update depth_i and search_i and target[place] as we move up and down the tree
        let mut search_i =
            self.context_index(&target[place + 1 - self.opt.context_columns..place + 1]);
        let mut depth_i = (place + 1 - self.opt.context_columns) * self.num_chunks;
        let mut unchecked_leafs = &table[search_i * self.num_chunks
            ..search_i * self.num_chunks + (self.num_chunks << self.opt.rows)];
        'search: loop {
            // The odd coding of the test below is to prompt rustc to autovectorize the
            // code.
            if parents[depth_i..depth_i + self.num_chunks]
                .chunks_exact(8)
                .zip(unchecked_leafs[..self.num_chunks].chunks_exact(8))
                .all(|(v1, v2)| {
                    let v_and = [
                        v1[0] & v2[0],
                        v1[1] & v2[1],
                        v1[2] & v2[2],
                        v1[3] & v2[3],
                        v1[4] & v2[4],
                        v1[5] & v2[5],
                        v1[6] & v2[6],
                        v1[7] & v2[7],
                    ];
                    v_and[0]
                        | v_and[1]
                        | v_and[2]
                        | v_and[3]
                        | v_and[4]
                        | v_and[5]
                        | v_and[6]
                        | v_and[7]
                        == 0
                })
            {
                // We haven't proved a continuation. Search deeper.
                if place + 1 < target.len() {
                    if parents_place + self.opt.context_columns <= place + 1 {
                        if self.term.load(Ordering::Relaxed) {
                            return true;
                        };
                        let parents = &mut parents[parents_place * self.num_chunks..];
                        let (place_parents, next_parents) = parents.split_at_mut(self.num_chunks);
                        self.advance(
                            target[parents_place],
                            place_parents,
                            &mut next_parents[..self.num_chunks],
                        );
                        if next_parents[..self.num_chunks].iter().all(|v| *v == 0) {
                            return false;
                        };
                        parents_place += 1;
                    }
                    // Because target[place] is not updated in the fast path
                    // we have to caclulate it here.
                    target[place] = (1 << self.opt.rows)
                        - (unchecked_leafs.len() as u32 >> self.log2_num_chunks);
                    search_i += target[place] as usize;
                    place += 1;
                    target[place] = 0;
                    search_i <<= self.opt.rows;
                    search_i &= (1 << (self.opt.rows * self.opt.context_columns as u32)) - 1;
                    depth_i += self.num_chunks;
                    unchecked_leafs = &table[search_i * self.num_chunks
                        ..search_i * self.num_chunks + (self.num_chunks << self.opt.rows)];
                    continue 'search;
                }
                // Exclude this parent
                return false;
            }

            // Move on to the next target
            if unchecked_leafs.len() >= 2 * self.num_chunks {
                // fast path
                unchecked_leafs = &unchecked_leafs[self.num_chunks..];
                continue 'search;
            }

            loop {
                place -= 1;
                if place < self.opt.context_columns {
                    return true; //retain parent
                }
                search_i += (target[place + 1 - self.opt.context_columns] as usize)
                    << (self.opt.context_columns as u32 * self.opt.rows);
                search_i >>= self.opt.rows;
                search_i -= target[place] as usize;
                depth_i -= self.num_chunks;

                parents_place = min(place, parents_place);

                target[place] += 1;
                if target[place] < 1 << self.opt.rows {
                    unchecked_leafs = &table[search_i * self.num_chunks
                        + target[place] as usize * self.num_chunks
                        ..search_i * self.num_chunks + (self.num_chunks << self.opt.rows)];
                    continue 'search;
                }
            }
        }
    }

    fn parents_to_remove(&self, target: &[u32], table: &[u64]) -> Box<[u32]> {
        let context_i = self.context_index(&target[..self.opt.context_columns as usize]);

        (0..64 * self.num_chunks as u32)
            .into_par_iter()
            .filter(|&parent| {
                table[context_i * self.num_chunks + ((parent as usize) >> 6)] & (1 << (parent & 63))
                    != 0
                    && self.equivalent_chunk
                        [(target[0] * (self.num_chunks as u32) + (parent >> 6)) as usize]
                        == parent >> 6
                    && !self.keep_parent(&*target, &*table, parent)
            })
            .collect::<Vec<u32>>()
            .into_boxed_slice()
    }

    /// A pass of the search algorithm.
    ///
    /// Returns true if any updates were made
    fn pass(&self, table: &mut [u64]) -> bool {
        let search_columns = self.opt.search_columns;
        let mut change = false;
        let mut target = vec![0u32; self.opt.context_columns + search_columns].into_boxed_slice();
        let mut mirrored_target = vec![0u32; self.opt.context_columns].into_boxed_slice();
        let mut mirrored_parents = vec![0u64; self.num_chunks].into_boxed_slice();

        'contexts: while !self.term.load(Ordering::Relaxed) {
            if target[1..self.opt.context_columns] <= mirrored_target[1..self.opt.context_columns] {
                let parents_to_remove: Vec<Box<[u32]>> = (0..1 << self.opt.rows)
                    .into_par_iter()
                    .map(|cells0| {
                        let mut target = target.clone();
                        target[0] = cells0;
                        self.parents_to_remove(&target[..], table)
                    })
                    .collect();

                for cells0 in 0u32..1 << self.opt.rows {
                    target[0] = cells0;
                    let context_i =
                        self.context_index(&target[..self.opt.context_columns as usize]);

                    let count_before = if self.opt.verbose {
                        table[context_i * self.num_chunks
                            ..context_i * self.num_chunks + self.num_chunks]
                            .iter()
                            .map(|chunk| chunk.count_ones())
                            .sum::<u32>()
                    } else {
                        0
                    };

                    let parents_to_remove = &parents_to_remove[cells0 as usize];
                    if !parents_to_remove.is_empty() {
                        change = true;
                        let table_entry = &mut table[context_i * self.num_chunks
                            ..context_i * self.num_chunks + self.num_chunks];

                        for parent in parents_to_remove.iter() {
                            table_entry[*parent as usize >> 6] &= !(1u64 << (*parent & 63));
                        }

                        //we didn't update any chunk that had an equivalent chunk so do so now
                        for chunki in 0..self.num_chunks {
                            if table_entry[chunki]
                                != table_entry[self.equivalent_chunk
                                    [(target[0] as usize) * self.num_chunks + chunki]
                                    as usize]
                            {
                                table_entry[chunki] &= table_entry[self.equivalent_chunk
                                    [(target[0] as usize) * self.num_chunks + chunki]
                                    as usize];
                            }
                        }
                    }

                    if self.opt.verbose {
                        let count_after = table[context_i * self.num_chunks
                            ..context_i * self.num_chunks + self.num_chunks]
                            .iter()
                            .map(|chunk| chunk.count_ones())
                            .sum::<u32>();
                        println!(
                            "parents:{:6} ({:6}), context: {:x?}",
                            count_after,
                            count_after as i32 - count_before as i32,
                            &target[..self.opt.context_columns],
                        );
                    }

                    mirrored_target[0] = cells0.reverse_bits() >> (32 - self.opt.rows);
                }
            };

            // Now updated the table entries for the mirrored parents
            for cells0 in 0u32..1 << self.opt.rows {
                target[0] = cells0;
                mirrored_target[0] = cells0.reverse_bits() >> (32 - self.opt.rows);
                let context_i = self.context_index(&target[..self.opt.context_columns as usize]);
                self.mirror_parents(
                    &table[context_i * self.num_chunks
                        ..context_i * self.num_chunks + self.num_chunks],
                    &mut mirrored_parents,
                );
                let mirrored_i = self.context_index(&mirrored_target);
                if table
                    [mirrored_i * self.num_chunks..mirrored_i * self.num_chunks + self.num_chunks]
                    != *mirrored_parents
                {
                    for (table_chunk, mirrored_chunk) in table[mirrored_i * self.num_chunks
                        ..mirrored_i * self.num_chunks + self.num_chunks]
                        .iter_mut()
                        .zip(mirrored_parents.iter())
                    {
                        *table_chunk &= *mirrored_chunk;
                    }
                    change = true;
                };
            }
            let mut place = self.opt.context_columns - 1;
            loop {
                target[place] += 1;
                mirrored_target[place] = target[place].reverse_bits() >> (32 - self.opt.rows);
                if target[place] < 1 << self.opt.rows {
                    break;
                } else if place > 1 {
                    //carry
                    target[place] = 0;
                    mirrored_target[place] = 0;
                    place -= 1;
                } else {
                    break 'contexts;
                };
            }
        }

        change
    }

    /// The full search.
    fn search(&self, table: &mut [u64], print_passes: bool) -> SearchResult {
        loop {
            let parent_count = table
                .iter()
                .map(|chunk| chunk.count_ones() as u64)
                .sum::<u64>();
            if print_passes {
                println!("working set size: {:15}", parent_count);
            };
            if parent_count == 0 {
                return SearchResult::Failed;
            }

            let change = self.pass(table);
            if self.term.load(Ordering::Relaxed) {
                return SearchResult::Interrupted;
            };
            if !change {
                return SearchResult::OrphansDisproved;
            }
        }
    }

    fn extend_solution(&self, table: &mut [u64]) -> io::Result<()> {
        let check_fragment = |parent: &[u32], context: &[u32]| {
            print!(" ");
            for col in parent {
                if col & 1 != 0 {
                    print!("*")
                } else {
                    print!(".")
                }
            }
            println!();
            print!("(");
            for col in parent {
                if col & 2 != 0 {
                    print!("*")
                } else {
                    print!(".")
                }
            }
            print!(", ");
            for col in context {
                if col & 1 != 0 {
                    print!("*");
                } else {
                    print!(".");
                }
            }
            let context_i = self.context_index(context);
            let idx = (parent[0] << (6 + self.parent_shift)) + parent[1];
            if table[context_i * self.num_chunks + (idx as usize >> 6)] & (1 << (idx & 63)) != 0 {
                println!(") is a member of the proof set: VERIFIED.");
            } else {
                println!(") is a member of the proof set: FAILED.");
            }
            for row in 2..self.opt.rows + 1 {
                print!(" ");
                for col in parent {
                    if col & (1 << row) != 0 {
                        print!("*")
                    } else {
                        print!(".")
                    }
                }
                print!("  ");
                for col in context {
                    if col & (1 << (row - 1)) != 0 {
                        print!("*")
                    } else {
                        print!(".")
                    }
                }
                println!();
            }
            print!(" ");
            for col in parent {
                if col & (1 << (self.opt.rows + 1)) != 0 {
                    print!("*")
                } else {
                    print!(".")
                }
            }
            println!();
        };

        let stdin = io::stdin();
        let mut lines = stdin.lock().lines();
        let mut target = Vec::<u32>::new();
        let mut parent = Vec::<u32>::new();

        println!("enter target:");
        for row in 0..self.opt.rows {
            let line = lines.next().unwrap()?;
            for (i, c) in line.chars().enumerate() {
                while target.len() <= i {
                    target.push(0);
                }
                if c == '*' {
                    target[i] |= 1 << row;
                }
            }
        }

        println!("enter partial solution:");
        for row in 0..self.opt.rows + 2 {
            let line = lines.next().unwrap()?;
            for (i, c) in line.chars().enumerate() {
                while parent.len() <= i {
                    parent.push(0);
                }
                if c == '*' {
                    parent[i] |= 1 << row;
                }
            }
        }
        while target.len() < parent.len() - 1 + self.opt.context_columns {
            target.push(0);
        }
        println!();
        println!("Precondition:");
        check_fragment(
            &parent[parent.len() - 2..],
            &target[parent.len() - 1..parent.len() - 1 + self.opt.context_columns],
        );

        let mut parents = vec![0u64; self.num_chunks].into_boxed_slice();
        let mut next_parents = vec![0u64; self.num_chunks].into_boxed_slice();

        for search_depth in 1..=self.opt.search_columns {
            while target.len() <= parent.len() + search_depth + self.opt.context_columns {
                target.push(0); // pad out target
            }
            let new_parent_len = parent.len() + search_depth;
            'extend_parent: while parent.len() < new_parent_len {
                let context_i = self.context_index(
                    &target[new_parent_len - 1..new_parent_len - 1 + self.opt.context_columns],
                );
                for candidate in 0..1 << (self.opt.rows + 2) {
                    for v in parents[..self.num_chunks].iter_mut() {
                        *v = 0;
                    }
                    let idx = (parent[parent.len() - 2] << (6 + self.parent_shift))
                        + parent[parent.len() - 1];
                    parents[(idx >> 6) as usize] |= 1 << (idx & 63);
                    self.advance(target[parent.len() - 1], &parents, &mut next_parents);
                    let idx = (parent[parent.len() - 1] << (6 + self.parent_shift)) + candidate;
                    if next_parents[(idx >> 6) as usize] & (1 << (idx & 63)) == 0 {
                        continue; // the candidate isn't consistent with the target;
                    }

                    // initialize parents and search.
                    for v in parents[..self.num_chunks].iter_mut() {
                        *v = 0;
                    }
                    parents[(idx >> 6) as usize] |= 1 << (idx & 63);

                    for parent_i in parent.len()..new_parent_len - 1 {
                        self.advance(target[parent_i], &parents, &mut next_parents);
                        mem::swap(&mut parents, &mut next_parents);
                    }
                    if parents
                        .chunks_exact(8)
                        .zip(
                            table[context_i * self.num_chunks
                                ..context_i * self.num_chunks + self.num_chunks]
                                .chunks_exact(8),
                        )
                        .any(|(v1, v2)| {
                            let v_and = [
                                v1[0] & v2[0],
                                v1[1] & v2[1],
                                v1[2] & v2[2],
                                v1[3] & v2[3],
                                v1[4] & v2[4],
                                v1[5] & v2[5],
                                v1[6] & v2[6],
                                v1[7] & v2[7],
                            ];
                            v_and[0]
                                | v_and[1]
                                | v_and[2]
                                | v_and[3]
                                | v_and[4]
                                | v_and[5]
                                | v_and[6]
                                | v_and[7]
                                != 0
                        })
                    {
                        parent.push(candidate);
                        continue 'extend_parent;
                    }
                }

                // We did not find a further context with conistent parents.
                break;
            }

            if parent.len() == new_parent_len {
                println!();
                println!("Longer partial solution found:");
                // We have found a longer partial solution
                for row in 0..self.opt.rows + 2 {
                    for col in parent.iter() {
                        if col & (1 << row) != 0 {
                            print!("*");
                        } else {
                            print!(".");
                        }
                    }
                    println!();
                }
                println!();
                println!("Postcondition:");
                check_fragment(
                    &parent[parent.len() - 2..],
                    &target[parent.len() - 1..parent.len() - 1 + self.opt.context_columns],
                );

                // Return early. If we do not return early it means no longer partial solution was found.
                return Ok(());
            }
        }
        println!("Failed to find longer partial solution.");
        println!("This can happen if the proof hasn't run to completion");
        println!("or if the precondition is not met.");
        Ok(())
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "orphans_disprover")]
struct Opt {
    /// Try to disprove existance of orphans with this many rows.
    #[structopt(short, long, default_value = "4")]
    rows: u32,

    /// Disprove Gardens of Eden rather than Orphans.
    #[structopt(long)]
    garden_of_eden: bool,

    /// Print additional information.
    #[structopt(short, long)]
    verbose: bool,

    /// Hensel notation rule string.
    #[structopt(long, default_value = "B3/S23")]
    rule: String,

    /// Number of columns in each context
    #[structopt(long, default_value = "6")]
    context_columns: usize,

    /// Number of columns to search ahead
    #[structopt(long, default_value = "17")]
    search_columns: usize,

    /// Data file, will be created if does not exist
    #[structopt(name = "FILE")]
    data_file: PathBuf,

    /// Extend a partial solution provided on stdin
    #[structopt(long)]
    extend_solution: bool,
}

fn main() {
    let opt = Opt::from_args();

    let search = Search::new(opt);
    signal_hook::flag::register(signal_hook::consts::SIGINT, Arc::clone(&search.term)).unwrap();

    let data_file = match OpenOptions::new()
        .read(true)
        .write(true)
        .open(&search.opt.data_file)
    {
        Ok(data_file) => {
            if !search.opt.extend_solution {
                println!("Resuming passes");
            }
            data_file
        }
        Err(_) => {
            let data_file = OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(&search.opt.data_file)
                .unwrap();
            data_file
                .set_len(
                    ((mem::size_of::<u64>() * search.num_chunks)
                        << (search.opt.rows * search.opt.context_columns as u32))
                        as u64,
                )
                .unwrap();
            let mut mapping = unsafe { MmapMut::map_mut(&data_file).unwrap() };
            let data_map = unsafe {
                std::slice::from_raw_parts_mut(
                    mapping.as_mut_ptr() as *mut u64,
                    search.num_chunks << (search.opt.rows * search.opt.context_columns as u32),
                )
            };
            for v in data_map.iter_mut() {
                *v = search.chunk_initialization
            }
            println!("Starting passes.");
            data_file
        }
    };
    let mut mapping = unsafe { MmapMut::map_mut(&data_file).unwrap() };
    let table = unsafe {
        std::slice::from_raw_parts_mut(
            mapping.as_mut_ptr() as *mut u64,
            search.num_chunks << (search.opt.rows * search.opt.context_columns as u32),
        )
    };

    if search.opt.extend_solution {
        search.extend_solution(table).unwrap();
    } else {
        match search.search(table, true) {
            SearchResult::Interrupted => {
                println!("Search interrupted.");
            }
            SearchResult::Failed => {
                if search.opt.garden_of_eden {
                    println!("failed to disprove gardens of eden");
                } else {
                    println!("failed to disprove orphans");
                }
            }
            SearchResult::OrphansDisproved => {
                if search.opt.garden_of_eden {
                    println!(
                        "There are no gardens of eden with {} rows in {}",
                        search.opt.rows, search.opt.rule
                    );
                } else {
                    println!(
                        "There are no orphans with {} rows in {}",
                        search.opt.rows, search.opt.rule
                    );
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_translate_rule() {
        for neighborhood in 0..0b1000000000 {
            assert_eq!(
                translate_rule("B1c/S")[neighborhood >> 3] & 1 << (neighborhood & 7) != 0,
                [0b100000000, 0b001000000, 0b000000100, 0b000000001].contains(&neighborhood)
            );
            assert_eq!(
                translate_rule("B1-c/S")[neighborhood >> 3] & 1 << (neighborhood & 7) != 0,
                [0b010000000, 0b000100000, 0b000001000, 0b000000010].contains(&neighborhood)
            );
            assert_eq!(
                translate_rule("B/S8")[neighborhood >> 3] & 1 << (neighborhood & 7) != 0,
                [0b111111111].contains(&neighborhood)
            );
        }

        assert_eq!(translate_rule("B012345678/S012345678"), [0xff; 64]);
    }

    #[test]
    fn test_5_row_reflect() {
        let opt = Opt::from_iter(["orphan_disprover", "dummy_file", "--rows", "5"].iter());
        let search = Search::new(opt);
        let mut parents = vec![0u64; search.num_chunks].into_boxed_slice();
        let mut mirrored_parents = parents.clone();
        let mut twice_mirrored = parents.clone();

        parents[0] = 1;
        parents[2] = 2;
        parents[4] = 4;
        search.mirror_parents(&parents, &mut mirrored_parents);
        assert_eq!(mirrored_parents[0], 1);
        //for --rows 5 bits with an odd address when mirrored add +1 to the chunk address.
        assert_eq!(mirrored_parents[128 + 1], 1);
        assert_eq!(mirrored_parents[64], 1u64 << 32);

        search.mirror_parents(&mirrored_parents, &mut twice_mirrored);

        assert_eq!(parents, twice_mirrored);
    }

    #[test]
    fn test_chunk_equivalent() {
        let opt = Opt::from_iter(["orphan_disprover", "dummy_file", "--rows", "5"].iter());
        let search = Search::new(opt);
        let mut parents = vec![0u64; search.num_chunks].into_boxed_slice();
        let mut equiv_parents = parents.clone();
        let mut next_parents = parents.clone();
        let mut next_equiv = parents.clone();

        for parent in 0..(1 << (2 * (search.opt.rows + 2))) {
            parents[parent >> 6] = 1 << (parent & 63);
            equiv_parents[search.equivalent_chunk[parent >> 6] as usize] = 1 << (parent & 63);
            for cells in 0..(1 << search.opt.rows) {
                search.advance(cells, &parents, &mut next_parents);
                search.advance(cells, &equiv_parents, &mut next_equiv);

                assert_eq!(next_parents, next_equiv);
            }
            parents[parent >> 6] = 0;
            equiv_parents[search.equivalent_chunk[parent >> 6] as usize] = 0;
        }
    }

    fn search_helper(parameters: &str, expected_result: SearchResult) {
        let opt = Opt::from_iter(
            ["orphan_disprover", "dummy_file"]
                .iter()
                .copied()
                .chain(parameters.split_whitespace()),
        );
        let search = Search::new(opt);
        let mut table = vec![
            search.chunk_initialization;
            search.num_chunks
                << (search.opt.rows * search.opt.context_columns as u32)
        ]
        .into_boxed_slice();
        assert_eq!(search.search(&mut table, false), expected_result);
    }

    #[test]
    fn test_1_row_life() {
        search_helper("--rule B3/S23 --rows 1", SearchResult::OrphansDisproved);
    }

    #[test]
    fn test_2_row_life() {
        search_helper("--rule B3/S23 --rows 2", SearchResult::OrphansDisproved);
    }

    #[test]
    fn test_3_row_no_orphans() {
        search_helper(
            "--rule B/S012345678 --rows 3 --context-columns 3 --search-columns 5",
            SearchResult::OrphansDisproved,
        );
    }

    #[test]
    fn test_3_row_no_garden_of_eden() {
        search_helper(
            "--rule B/S012345678 --rows 3 --context-columns 3 --search-columns 5 --garden-of-eden",
            SearchResult::OrphansDisproved,
        );
    }

    #[test]
    fn test_4_row_no_orphans() {
        search_helper(
            "--rule B/S012345678 --rows 4 --context-columns 3 --search-columns 5",
            SearchResult::OrphansDisproved,
        );
    }

    #[test]
    fn test_5_row_no_orphans() {
        search_helper(
            "--rule B/S012345678 --rows 5 --context-columns 2 --search-columns 5",
            SearchResult::OrphansDisproved,
        );
    }

    #[test]
    fn test_1_row_orphans() {
        search_helper("--rule B8/S7 --rows 1", SearchResult::Failed);
    }

    #[test]
    fn test_2_row_orphans() {
        search_helper("--rule B1/S8 --rows 2", SearchResult::Failed);
    }

    #[test]
    fn test_3_row_orphans() {
        search_helper(
            "--rule B1/S8 --rows 3 --context-columns 3 --search-columns 5",
            SearchResult::Failed,
        );
    }

    #[test]
    fn test_3_row_garden_of_eden() {
        search_helper(
            "--rule B1/S8 --rows 3 --context-columns 3 --search-columns 5 --garden-of-eden",
            SearchResult::Failed,
        );
    }

    #[test]
    fn test_4_row_orphans() {
        search_helper(
            "--rule B1/S8 --rows 4 --context-columns 3 --search-columns 5",
            SearchResult::Failed,
        );
    }

    #[test]
    fn test_5_row_orphans() {
        search_helper(
            "--rule B1/S8 --rows 5 --context-columns 2 --search-columns 5",
            SearchResult::Failed,
        );
    }
}
